from flask import request, redirect, url_for, Blueprint, render_template, flash
from website.models import User
from werkzeug.security import generate_password_hash, check_password_hash
from website import db
from flask_login import login_user, login_required, logout_user, current_user

auth = Blueprint('auth', __name__, static_folder='static', template_folder='template')

@auth.route('/login', methods=["GET", "POST"])
def login():
    if request.method == 'POST':
        email = request.form.get('email')
        password = request.form.get('password')
        
        user = User.query.filter_by(email=email).first()
        if user:
            if check_password_hash(user.password, password):
                flash('Logged in successfully!', category='success')
                login_user(user, remember=True)
                return redirect(url_for('views.index'))
            else:
                flash('Incorrect password.', category='error')
        else:
            flash('Email does not exist.', category='error')
                
        
    return render_template("login.html", user=current_user)

@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('auth.login'))

@auth.route('/sign-up', methods=["GET", "POST"])
def sign_up():
    if request.method == 'POST':
        email = request.form.get('email')
        firstName = request.form.get('firstName')
        password = request.form.get('password')
        passwordValid = request.form.get('passwordValid')

        user = User.query.filter_by(email=email).first()

        if user:
            flash("Email already exists.", category='error')
        elif len(email) < 4:
            flash("Email is too short. Enter email that is greater than 4 characters.", category='error')
        elif len(firstName) < 2:
            flash("First name is too short. Enter first name that is greater than 2 characters.", category='error')
        elif password != passwordValid:
            flash("Passwords don't match.", category='error')
        elif len(password) < 7:
            flash("Password is too short. Enter password that is greater than 7 characters.", category='error')
        else:
            new_user = User(email=email, 
                            first_name=firstName, 
                            password=generate_password_hash(password, method='sha256')
                            )
            db.session.add(new_user)
            db.session.commit()
            login_user(user, remember=True)
            flash("Account created", category="success")
            return redirect(url_for("views.index"))

    return render_template("sign_up.html", user=current_user)